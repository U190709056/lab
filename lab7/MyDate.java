public class MyDate {

    private int day, month, year;

    int[] maxDays = {31,29,31,30,31,30,31,31,30,31,30,31};

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month - 1;
        this.year = year;
    }

    public void incrementDay() {
        int maxDay = maxDays[month];
        int newDay = day + 1;

        if (newDay > maxDay){
            incrementMonth();
            day = 1;
        }else if (month == 1 && newDay == 29 && !leapYear()){
            incrementMonth();
            day = 1;
        }else {
            day = newDay;
        }
    }

    public boolean leapYear(){
        return year % 4 == 0 ? true: false;
    }

    public void incrementYear(int diff) {
        year += diff;
        if (month == 1 && day == 29 && !leapYear()){
            day = 28;
        }
    }

    public void decrementDay() {
        int newDay = day - 1;
        if (newDay == 0){
            day = 31;
            decrementMonth();
        }else {
            day = newDay;
        }
    }

    public void decrementYear() {
        incrementYear(-1);
    }

    public void decrementMonth() {
        incrementMonth(-1);
    }

    public void incrementDay(int diff) {
        while (diff>0){
            incrementDay();
            diff--;
        }
    }

    public void decrementMonth(int month) {
        incrementMonth(-month);
    }

    public void decrementDay(int diff) {
        while (diff>0){
            decrementDay();
            diff--;
        }
    }

    public void incrementMonth(int diff) {
        int newMonth = (month + diff) % 12;
        int yearDiff = 0;

        if (newMonth < 0){
            newMonth += 12;
            yearDiff = -1;
        }
        yearDiff += (month + diff)/12;

        month = newMonth;
        year += yearDiff;

        if(day > maxDays[month]){
            day = maxDays[month];
            if (month == 1 && day == 29 && !leapYear()){
                day = 28;
            }
        }
    }

    public void decrementYear(int diff) {
        incrementYear(-diff);
    }

    public void incrementMonth() {
        incrementMonth(1);
    }

    public void incrementYear() {
        incrementYear(1);
    }

    public boolean isBefore(MyDate anotherDate) {
        if ((year < anotherDate.year) || (year == anotherDate.year && month < anotherDate.month) || (year == anotherDate.year && month == anotherDate.month  && day < anotherDate.day)){
            return true;
        }else{
            return false;
        }
    }

    public boolean isAfter(MyDate anotherDate) {
        return !isBefore(anotherDate);
    }

    public int dayDifference(MyDate anotherDate) {
        int count = 0;
        MyDate tempDate = new MyDate(day,month + 1,year);
        MyDate tempDate2 = new MyDate(anotherDate.day,anotherDate.month + 1,anotherDate.year);

        while(!(tempDate.year == tempDate2.year && tempDate.month == tempDate2.month && tempDate.day == tempDate2.day)){
            if (tempDate.isAfter(tempDate2)) {
                tempDate.decrementDay();
                count++;
            }else{
                tempDate2.decrementDay();
                count++;
            }
        }
        return count;
    }

    public String toString(){
        return year + "-" + ((month+1)<10 ? "0" : "") + (month+1) + "-" + (day < 10 ? "0" : "") + day;
    }
}
