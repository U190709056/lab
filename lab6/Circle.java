public class Circle {

    int radius;
    Point center;


    public Circle(int radius, Point center) {
        this.radius = radius;
        this.center = center;
    }

    public double area(){
        return Math.PI * this.radius * this.radius;
    }

    public double perimeter(){
        return 2 * Math.PI * this.radius;
    }

    public boolean intersect(Circle circle){
        return this.radius + circle.radius >= this.center.distanceFromAPoint(circle.center);

    }

}
