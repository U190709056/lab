import org.w3c.dom.css.Rect;

public class TestRectangle {

    public static void main(String[] args){
        Point topLeft = new Point(10,10);
        Rectangle rect = new Rectangle(5,6,topLeft);

        System.out.println("Rectangle Area = " + rect.area() + " Rectangle Perimeter = " + rect.perimeter());
        
        Point[] corners = rect.corners();
        for (int i = 0; i < corners.length; i++){
            System.out.println("Corner" + (i+1) + " at x " + corners[i].xCoord + " and at y " + corners[i].yCoord);
        }

    }
}
