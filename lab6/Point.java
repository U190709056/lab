public class Point {

    int xCoord;
    int yCoord;

    public Point(int xCoord,int yCoord){
        this.xCoord = xCoord;
        this.yCoord = yCoord;
    }

    public double distanceFromAPoint(Point point){
        return Math.sqrt(Math.pow(this.xCoord - point.xCoord,2) + Math.pow(this.yCoord - point.yCoord,2));
    }


}
